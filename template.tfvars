# Main region
region = "us-east-1"

# Main subnet id
subnet_id = "subnet-123"

# VPC where infrastructure will be created
vpc_id = "vpc-123456789"

# Availability zones for nodes will be created
availability_zones = ["us-east-1a","us-east-1b","us-east-1c"]

# Subnets EC2 instances will be assigned to in each availability zone. 
# There must be one entry for each item in the "availability_zones" list.
subnets = {
    "us-east-1a" = "subnet-123"
    "us-east-1b" = "subnet-456"
    "us-east-1c" = "subnet-789"
}

# AMI ID for image used to create EC2 instances. 
# This should be the ID of the image created using Packer.
ami_id = "ami-0987654"

# EC2 instance type
instance_type = "t2.micro"

# EBS volume size (in gigabytes) for mounted volume
ebs_volume_size = 1000
# SSH key name
key_name = "mongo_nodes"
