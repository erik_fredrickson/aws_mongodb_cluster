#!/bin/bash
DEV=/dev/xvdg
MOUNT_POINT=/mnt/mongodb_logging

mkfs -t xfs $DEV
mkdir $MOUNT_POINT
mount $DEV $MOUNT_POINT
echo $DEV $MOUNT_POINT xfs defaults,nofail 0 2 >> /etc/fstab
mkdir $MOUNT_POINT/logs
chown mongod:mongod $MOUNT_POINT/logs