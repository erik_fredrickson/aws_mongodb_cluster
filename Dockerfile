FROM ubuntu
# Add mongodb PPA for 3.4 and install
RUN \
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 && \
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" \
| tee /etc/apt/sources.list.d/mongodb-org-3.4.list && \
apt update && apt install mongodb-org -y

# mongodb port
EXPOSE 27017
# mongos/cluster port
EXPOSE 27019
# server status port
EXPOSE 28017

VOLUME ["/data/db"]
VOLUME ["/var/log/mongodb"]
WORKDIR /data

# Run with "--rest" for server status
CMD ["mongod", "--rest", "--logpath", "/var/log/mongodb/mongodb.log"]

# TODO Cluster configuration, configsvr causing service start failure
# CMD ["mongod", "--rest", "--configsvr"]
# RUN mongos --configdb us-east-1a.test.com:27019,us-east-1b.test.com:27019,us-east-1c.test.com:27019
