# MongoDB Cluster Architecture

Provisions 3-node MongoDB clusters in AWS. Running this will create:

  - One Amazon Machine Image (AMI) running a MongoDB Docker container. 
  - One new security group named "mongodb_node_security"
  - EC2 Instances, each with mounted EBS volumes for storing logs from the MongoDB containers.
  - Updated Route53 record with public IP addresses for the 3 instances created.

There are essentially two main components to this project, detailed in the "Running" section:

  - First, is the creation of a new AMI hosting the MongoDB container using Packer.
  - Second, is the creation of the AWS infrastructure using Terraform and the AMI created in the first part.

## Prerequisites

- AWS IAM user(s) with API key and with policies:
    - AmazonEC2FullAccess (or see https://www.packer.io/docs/builders/amazon.html for more details on IAM permissions needed).
    - AmazonRoute53DomainsFullAccess
- Linux machine with Packer (https://www.packer.io/), Terraform (https://www.terraform.io/), and git installed.

## Downloading

Clone the git repo:
``` bash
$ git clone https://bitbucket.org/erik_fredrickson/aws_mongodb_cluster.git
```
## Components

- base_ami.json - The configuration file used by Packer to generate the AMI.
- Dockerfile - Configuration file used for building the MongoDB container, as part of the Packer provisioning process.
- main.tf - Terraform configuration file used for creating AWS infrastructure: EC2 instances, EBS volumes, and security group.
- mount_ebs.sh - Shell script used by Terraform to mount the EBS volume on the EC2 instances at /mnt/mongodb_logging.
- provisioner.sh - Shell script used by Packer to install Docker and set up the MongoDB container on the instance used for generating the new AMI.
- template.tfvars - Reference Terraform variable file used for creating new configuration files.

## Running

### Generate AMI using Packer

1. Select a base AMI from the AWS Marketplace (currently only CentOS 7 is supported).
2. In the "aws_mongodb_cluster" directory, generate the new AMI by running the following command from the terminal (replacing variable values placeholders with your values):
``` bash
$ packer build \
  -var 'aws_access_key=<YOUR ACCESS KEY>' \
  -var 'aws_secret_key=<YOUR SECRET KEY>' \
  -var 'vpc_id=<YOUR VPC ID>' \
  -var 'subnet_id=<YOUR SUBNET ID>' \
  -var 'instance_type=<INSTANCE TYPE>' \
  -var 'source_ami=<BASE AMI ID>' \
  -var 'ssh_username=<*SSH USERNAME FOR AMI>' \
    base_ami.json
```
Successful execution will return a result similar to below, containg the ID of the new AMI:
``` bash
--> amazon-ebs: AMIs were created:
us-east-1: ami-eeee3333
```

* (See: https://alestic.com/2014/01/ec2-ssh-username/ for default user names for Linux images)

### Create infrastructure and instances with Terraform using AMI

After generating the AMI using Packer:

1. Create a new .tfvars file, using template.tfvars as a reference.
2. In the new .tfvars file there needs to be a corresponding entry in the "subnets" map for each entry in the "availability_zones" list.
3. In the "aws_mongodb_cluster" directory, run "terraform init" in the terminal.
4. Create the AWS infrastructure by running the following in the terminal:

``` bash
terraform apply \
  -var 'access_key=<YOUR ACCESS KEY>' \
  -var 'secret_key=<YOUR SECRET KEY>' \
  -var-file="<YOUR TFVARS FILE>.tfvars"
```

After Terraform finishing running, the instances should be available on ports 27017, 28017, & 27019.
The MongoDB status page should be available at http://<public_ip>:28017.
