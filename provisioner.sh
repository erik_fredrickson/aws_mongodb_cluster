#!/bin/sh
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce
sudo systemctl enable docker
sudo systemctl start docker

cd /tmp
sudo docker build -t mongodb-node .
sudo mkdir /mnt/mongodb_logging
sudo docker run --restart=always -d -v /mnt/mongodb_logging:/var/log/mongodb:Z -p 27017:27017 -p 27019:27019 -p 28017:28017  mongodb-node
