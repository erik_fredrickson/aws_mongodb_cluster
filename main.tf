# TODO Tagging
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}

variable "vpc_id" {
    default = "vpc-123456789"
}

variable "availability_zones" {
    default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "subnets" {
    type    = "map"
    default = {
        "us-east-1a" = "subnet-123"
        "us-east-1b" = "subnet-456"
        "us-east-1c" = "subnet-789"
    }
}

variable "subnet_id" {
    default = "subnet-123"
}

variable "key_name" {
    default = "mongo_nodes"
}

variable "ami_id" {
    default = "ami-0987654"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "ebs_volume_size" {
    default = 1000
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "mongodb_cluster_security" {
  name        = "mongodb_cluster_security"
  description = "Allow all inbound traffic"
  vpc_id      = "${var.vpc_id}"

  # Normally wouldn't expose this to everybody
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 27019
    to_port     = 27019
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 28017
    to_port     = 28017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "mongodb_node_security"
  }
}

resource "aws_instance" "mongodb_cluster_node" {
  count             = "${length(var.availability_zones)}"
  ami               = "${var.ami_id}"
  instance_type     = "${var.instance_type}"
  availability_zone = "${var.availability_zones[count.index]}" 
  subnet_id         = "${var.subnets[var.availability_zones[count.index]]}"
  
  root_block_device = {}

  ebs_block_device {
    device_name           = "/dev/sdg"
    volume_type           = "gp2"
    volume_size           = "${var.ebs_volume_size}"
    delete_on_termination = true
  }
 
  key_name          = "${var.key_name}"
  security_groups   = ["${aws_security_group.mongodb_cluster_security.id}"]

  provisioner "file" {
    source      = "mount.sh"
    destination = "/tmp/mount.sh"
  }
}

resource "aws_route53_record" "test" {
  zone_id = "test"
  name    = "www.test.com"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.mongodb_node.*.public_ip}"]
}
